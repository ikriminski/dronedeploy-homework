>This project is an assignment for DroneDeploy.
>The goal of this project is to determine the pose of a camera from a set of images containing a known pattern.
>This project relies on openCV 3.2.0 compiled with opencv_contrib libraries.
>
>
>To run:
>
>Download entire repo.
>
>Run processImages.py (Please wait for all images to render; after, you may press any key to exit)
>
>This will generate a data.json file that contains calculated camera coordinates and rotations.(A data.json file is provided with the repo. This file was generated using the images that were part of the dataset for the assignment.)
>
>Run index.html in web browser. This will bring up an interactive demo.(You can use your mouse to control the camera attitude when using the free camera)
>
>Notes:
>
>processImages.py is CPU intensive(~45 sec)