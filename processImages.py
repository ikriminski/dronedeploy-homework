import numpy as np
import json
import cv2
import math
from matplotlib import pyplot as plt
from scipy.spatial import distance

print "Please wait while images are generated."
filenames = ['IMG_6719.JPG', 'IMG_6720.JPG', 'IMG_6721.JPG', 'IMG_6722.JPG', 'IMG_6723.JPG', 'IMG_6724.JPG', 'IMG_6725.JPG', 'IMG_6726.JPG', 'IMG_6727.JPG']
patternFname = 'pattern.jpg'
img2 = cv2.imread('images/'+patternFname,0) #pattern image
pattern_vertices=np.array([[0,0],[img2.shape[0], 0], [0,img2.shape[1]],[img2.shape[0],img2.shape[1]]], dtype = np.float32)
pattern_vertices3D = np.hstack((pattern_vertices,np.zeros((pattern_vertices.shape[0],1))))
MIN_MATCH_COUNT = 10
#camera matrix and distortion parameters (approx)
#ideally, to improve accuracy, these should be calculated on native device
mat = np.float32([[2798, 0, 1224], [0, 2663, 1632], [0, 0, 1]])
distortion = None # np.float32([2.3628479801842187e-01, -1.4838185680077374e+00, 6.1164837177226554e-03, -1.3917464527942961e-02, 2.3228740041746385e+00])

# Initiate SIFT detector
sift = cv2.xfeatures2d.SIFT_create()
camera_rvecs= np.array([[-0.25, 0.5, 2]], dtype = np.float32)
camera_tvecs= np.array([[164, -1200, 10000]], dtype= np.float32)
out = []
for filename in filenames:
	img1 = cv2.imread('images/'+filename,0) 	
	# find the keypoints and descriptors with SIFT
	kp1, des1 = sift.detectAndCompute(img1,None)
	kp2, des2 = sift.detectAndCompute(img2,None)
	
	FLANN_INDEX_KDTREE = 0
	index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
	search_params = dict(checks = 50)
	
	flann = cv2.FlannBasedMatcher(index_params, search_params)
	
	matches = flann.knnMatch(des1,des2,k=2)
	
	# store all the good matches as per Lowe's ratio test.
	good = []
	for m,n in matches:
	    if m.distance < 0.7*n.distance:
	        good.append(m)
	if len(good)>MIN_MATCH_COUNT:
		src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ])
		dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ])
		
		#find center of image in pattern coordinates
		homo, mask = cv2.findHomography(src_pts, dst_pts, 8, 3.0)
		center = cv2.perspectiveTransform(np.array([[[img1.shape[1]/2,img1.shape[0]/2 ]]], dtype=np.float32), homo)
		
		
		
		
		
		dst_pts = np.hstack((dst_pts,np.zeros((dst_pts.shape[0],1))))
	    #find camera rotation and translation vectors relative to pattern
		valid, rvecs, tvecs, inliers = cv2.solvePnPRansac(dst_pts, src_pts, mat, distortion)
		Rt, jac = cv2.Rodrigues(rvecs)
		R = Rt.T
		#get position
		pos = (-R).dot(tvecs)
		Rt = np.hstack((Rt, tvecs))
		#get angles
		cameraMatrix, rotMatrix, transVect, rotMatrixX, rotMatrixY, rotMatrixZ, eulerAngles = cv2.decomposeProjectionMatrix(Rt)
		out.append([pos[0][0],pos[1][0],pos[2][0], eulerAngles[0][0], eulerAngles[1][0], eulerAngles[2][0], filename])

		#camera position in 3d pattern coordinates
		camera_pos_3D = np.array([[pos[0][0],pos[1][0],pos[2][0]]])
		#find locations of pattern vertices in viewing plane coordinates
		imagePoints, jac = cv2.projectPoints(pattern_vertices3D,camera_rvecs, camera_tvecs, mat, distortion);
		#find location of camera in vieweing plane coordinates
		camera_pos_2D, jac = cv2.projectPoints(camera_pos_3D, camera_rvecs, camera_tvecs, mat, distortion)
		#find transformation for pattern image to view plane
		viewHomo, mask = cv2.findHomography(pattern_vertices, imagePoints, 8, 3.0)
		#transform and project pattern to view plane
		outImg = cv2.warpPerspective(img2, viewHomo, (2448, 3264));
		#transfrom center of image from camera to view plane
		center_in_view = cv2.perspectiveTransform(center, viewHomo);
		
		center_in_view = center_in_view.astype(int)
		camera_pos_2D = camera_pos_2D.astype(int)
		
		#draw view
		text1 = 'x(cm)=' + str(pos[0][0]/37.5) + ', y(cm)=' + str(pos[1][0]/37.5) + \
			', z(cm)=' + str(pos[2][0]/37.5) 
		text2= 'rx=' + str(180-eulerAngles[0][0]) + \
			', ry=' + str(eulerAngles[1][0]) + ', rz=' + str(eulerAngles[2][0]) + ' (threeJS angles)'
		cv2.putText(outImg, text1, (0,outImg.shape[0]-5), cv2.FONT_HERSHEY_SIMPLEX,2, (255,255,255))
		cv2.putText(outImg, text2, (0,outImg.shape[0]-75), cv2.FONT_HERSHEY_SIMPLEX, 1.5, (255,255,255))
		cv2.line(outImg, tuple(center_in_view[0].ravel()), tuple(camera_pos_2D[0].ravel()), (255,255,255), 5)
		cv2.namedWindow(filename, cv2.WINDOW_NORMAL)
		cv2.imshow(filename, outImg);
		cv2.resizeWindow(filename, img1.shape[1]/4,img1.shape[0]/4)

	else:
	    print "Not enough matches are found - %d/%d" % (len(good),MIN_MATCH_COUNT)
	    matchesMask = None
	
	
	
with open('data.json', 'w') as outfile:
    json.dump(out, outfile)
cv2.waitKey(0)
	


